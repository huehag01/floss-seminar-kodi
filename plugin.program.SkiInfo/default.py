# -*- coding: utf-8 -*-

import xbmc, xbmcgui, xbmcplugin, xbmcaddon
import re, os
import urllib, urllib2, urlparse
import pickle

__addon__       = xbmcaddon.Addon(id='plugin.program.skiinfo')
__icon__        = __addon__.getAddonInfo('icon')
pluginhandle = int(sys.argv[1])
basefolder = sys.argv[0]
baseurl = "http://meteo.search.ch"
args = urlparse.parse_qs(sys.argv[2][1:])
rootfolderpath = __addon__.getAddonInfo('path')

translation = __addon__.getLocalizedString
cachefile = os.path.join(rootfolderpath,"cache.txt")

hitlist = []
#Creates popup, useful for debugging
def showDebugPopup(text):
    title = "Debug"
    time = 5000  # ms
    xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(title, text, time, __icon__))

#Connects to the website, parses it and fills the global list hitlist
#with the needed tablerows.
#Also tries to write to the local cachefile. 
#If the urlopen throws an exception, the addon tries to open the local cachefile to be independant of the network connection.
def mainParser(hlist):
    try:
        url = baseurl + "/snow"
        headers = {'Accept-Language' : 'en-US'}
        req = urllib2.Request(url, None, headers)
        response = urllib2.urlopen(req)
        fil = open(cachefile,'w')
        html = response.read()
        response.close()
        table = re.findall(r'<tr>(.*?)</tr>', html, re.M|re.I|re.S)
        #Pop unneeded rows    
        table.pop()
        table.pop(0)
        table.pop(0)
        table.pop(0)
        table.pop(0)
        for elem in table:
            match = re.compile('<td><a href="(.+?)">(.+?)</a></td>\n<td>(.+?)</td>\n<td>(.+?)</td>\n<td>(.+?)</td>\n<td>(.+?)</td>\n<td>(.+?)</td>')
            hlist.append(match.findall(elem))
        pickle.dump(hlist,fil)

    except urllib2.URLError:
        xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%((translation(30005)).encode("utf-8"), (translation(30006)).encode("utf-8"), 5000, __icon__))
        fil = open(cachefile,'r')
        global hitlist
        hitlist = pickle.load(fil)

#connects to the site of a singe destination, parses and returns the html of it
def destParser(link):
    try:
        headers = {'Accept-Language' : 'en-US'}
        req = urllib2.Request(link, None, headers)
        response = urllib2.urlopen(req)
        html = response.read()
        response.close()
        return html
    except urllib2.URLError:
        xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%((translation(30005)).encode("utf-8"), (translation(30007)).encode("utf-8"), 5000, __icon__))

def buildQuery(query):
    return basefolder + '?' + urllib.urlencode(query)

def CreateDirectory(title,link):
    lis=xbmcgui.ListItem(label=title,label2="",iconImage="")
    lis.setProperty('IsPlayable', 'false')
    ret=xbmcplugin.addDirectoryItem(handle=pluginhandle,url=link,listitem=lis,isFolder=True)
    return ret

def CreateImageEntry(title,link):
    lis=xbmcgui.ListItem(label=title,label2="",iconImage="")
    lis.setProperty('IsPlayable', 'true')
    lis.setInfo( type="Picture", infoLabels={ "Title": unicode(title) })
    ret=xbmcplugin.addDirectoryItem(handle=pluginhandle,url=link,listitem=lis,isFolder=False)
    return ret

#Returns correct condition string
def getCondition(field):
    if field.find("wet") is not -1:
        return (translation(30012)).encode("utf-8")
    if field.find("hard") is not -1:
        return (translation(30013)).encode("utf-8")
    if field.find("powder") is not -1:
        return (translation(30014)).encode("utf-8")
    return "-"

def MainMenu():
    mainParser(hitlist)
    for elem in hitlist:
        condition = getCondition(str(elem[0][3]))
        #Check open/closes state
        state = ""
        if "green" not in elem[0][6]:
            state = (translation(30000)).encode("utf-8")
        else:
            state = (translation(30001)).encode("utf-8")
        instals = str(elem[0][4]).replace("of",(translation(30011).encode("utf-8")))
        #Build title string
        titel = str(elem[0][1]) + " - " + instals + " - " + state
        #Build query
        lru = buildQuery({'mode' : 'destination', 'name' : elem[0][1], 'url' : elem[0][0], 'hohe' : elem[0][2], 'price' : elem[0][5], 'inst' : titel, 'cond' : condition})
        CreateDirectory(titel,lru)
    xbmcplugin.addSortMethod(pluginhandle, xbmcplugin.SORT_METHOD_LABEL )
    #End directorylisting
    xbmcplugin.endOfDirectory(pluginhandle)

def DestinationView(name):
    link = baseurl + str(args.get('url', None)[0])
    
    #Name element
    nname = str(args.get('inst', None)[0])
    CreateDirectory(nname,link)

    #Show Snowheight element if not "-"
    if str(args.get('hohe', None)[0]) is not "-":
        shohe = (translation(30002)).encode("utf-8") + " : " + str(args.get('hohe', None)[0])
        CreateDirectory(shohe,link)

    #Show Condition element if not "-"
    if str(args.get('cond', None)[0]) is not "-":
        cond = (translation(30015)).encode("utf-8") + " : " + str(args.get('cond', None)[0])
        CreateDirectory(cond,link)
    
    #Show Price element if not "-"
    if str(args.get('price', None)[0]) is not "-":
        sprice = (translation(30003)).encode("utf-8") + " : " + str(args.get('price', None)[0])
        CreateDirectory(sprice,link)

    #Get html of single destination page
    desthtml = destParser(link)
    
    #Find and list altitude
    table = re.findall('in resort</th>(.*?)</tr>(.*?)<tr>(.*?)<td>(.+?)</td>(.*?)<td>(.+?)</td>(.*?)<td>(.+?)</td>(.*?)<td>(.+?)</td>(.*?)',desthtml, re.DOTALL)

    altup = altdown = altres = ""
    if table[0][5] is not "-":
        altup = (translation(30008)).encode("utf-8") + ": " + table[0][5]
    if table[0][7] is not "-":
        altdown = (translation(30009)).encode("utf-8") + ": " + table[0][7]
    if table[0][9] is not "-" :
        altres = (translation(30010)).encode("utf-8") + ": " + table[0][9]
    altname = altup + " " + altdown + " " + altres
    CreateDirectory(altname, "")
    
    
    #Find and list skimap
    img = re.findall('href="(.+?)"><img src="(.+?)" alt="" class="snowskimap"></a>',desthtml)
    if img:
        imgurl = baseurl + str(img[0][0])
        CreateImageEntry((translation(30004)).encode("utf-8"),imgurl)
    #Find and list webcams
    img = re.findall('href="(.+?)" class="lightbox"><img title="(.+?)"',desthtml)
    if img:
        CreateDirectory("    Webcams","")
        for elem in img:
            webcurl = baseurl + str(elem[0])
            webctitle = elem[1].decode('utf-8')
            CreateImageEntry(webctitle,webcurl)
    #End directorylisting
    xbmcplugin.endOfDirectory(pluginhandle)
 
#Read mode from query
try:
    mode = args.get('mode', None)
except Exception:
    showDebugPopup('Could not retrieve args')
    MainMenu()

#Decide which viewmode to display
if mode is None:
    MainMenu()
elif mode[0] == 'destination':
    try:
        destname = args.get('name', None)
        DestinationView(destname)
    except Exception:
        showDebugPopup((translation(30007)).encode("utf-8"))
else:
    MainMenu()


